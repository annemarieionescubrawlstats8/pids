#!/bin/bash
if [ "$(id -u)" = 0 ]; then
echo "
 _    _      _                            _         ______ _____ _____ _____
| |  | |    | |                          | |        | ___ \_   _/  ___/  ___|
| |  | | ___| | ___ ___  _ __ ___   ___  | |_ ___   | |_/ / | | \ `--.\ `--.
| |/\| |/ _ \ |/ __/ _ \| '_ ` _ \ / _ \ | __/ _ \  |  __/  | |  `--. \`--. \
\  /\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | | |    _| |_/\__/ /\__/ /
 \/  \/ \___|_|\___\___/|_| |_| |_|\___|  \__\___/  \_|    \___/\____/\____/


______                         _____                                          _        _
|  _  \                       |  ___|                                        | |      | |
| | | |___   ___  _ __ ___    | |__ _ __ ___   __ _  ___ ___   _ __ ___   ___| | _____| |
| | | / _ \ / _ \| '_ ` _ \   |  __| '_ ` _ \ / _` |/ __/ __| | '__/ _ \ / __| |/ / __| |
| |/ / (_) | (_) | | | | | |  | |__| | | | | | (_| | (__\__ \ | | | (_) | (__|   <\__ \_|
|___/ \___/ \___/|_| |_| |_|  \____/_| |_| |_|\__,_|\___|___/ |_|  \___/ \___|_|\_\___(_)


"
echo "DO NOT RUN THIS AS ROOT/SUDO"
echo "By running this script you consent to your system being modified."
fi
error() { \
    clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}
echo "Time to pick the driver!"
echo "The xf86-video-nouveau driver is open-source NVIDIA driver"
read -p "Choose a driver: xf86-video-amdgpu, xf86-video-ati, xf86-video-intel, xf86-video-nouveau"

echo "Installing Stage 1 packages"
pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
echo "Built Yay!!!"
cd ~
yay -S xorg-server lightdm htop gotop firefox mplayer grapejuice-git emacs awk fish neofetch
echo "Installing video drivers"
yay -S $x
echo "Installed Video Drivers"
echo "Installing Stage 2 Packages"
git clone https://gitlab.com/annemarieionescubrawlstats8/dwm.git
cd dwm
sudo make clean install
cd dwmblocks
sudo make clean install
cd ~


echo "Installing the base stuff is done, time to install emacs."
echo "This will take a while......"
yay -S neovim nerd-fonts-fira-code 
git clone --depth 1 -b develop https://github.com/NTBBloodbath/doom-nvim.git ${XDG_CONFIG_HOME:-$HOME/.config}/nvim
echo "Installed Doom Emacs!"

echo "Installing wine!"

yay -S wine-staging

wget https://pastebin.com/raw/5SeVb005 -O /tmp/grapejuice-wine-tkg.py
python3 /tmp/grapejuice-wine-tkg.py

echo "Wine installation finished!"

echo "Installation almost finished... Its time to configure some base stuff like shell configuration."
git clone https://gitlab.com/annemarieionescubrawlstats8/dotfiles-piss.git
cd dotfiles-piss
curl -L https://get.oh-my.fish | fish
mv fish ~/.config/fish
cd ~
echo "Time for wallpaper!!"
yay -S feh
echo "Setting wallpaper!"
mkdir ~/Pictures/wallpapers
mv ~/dotfiles-piss/arch.jpg ~/Pictures/wallpapers/
echo "Done! Everytime you start DWM you need to pray mod + m to set your wallpaper. (mod = windows key)"

echo "Time for fancy shit"
yay -S picom
cd dotfiles-piss
mv picom.conf ~/.config/picom.conf
cd ~
echo "Now you got fancy terminals congratulations!"
echo "But wait we don't have any terminal installed??"
echo "Lets install one then!"
git clone https://gitlab.com/dwt1/st-distrotube.git
cd st-distrotube
sudo make clean install
cd ~
echo "Installing Pipewire"
yay -S pipewire pipewire-alsa pipewire-pulse lib32-pipewire
echo "Installing last part: custom kernel buisness!"
yay -S linux-xanmod
echo "Its all done!"
echo "Its time to enjoy some relaxing music and drink some coffe.."
mplayer ~/st/yt5s.com\ -\ East\ vs\ West\ Soundtrack\ -\ Spanish\ Heart\ \(320\ kbps\).mp3
reboot

# PISS

Post Install Script for Arch Linux/Endeavour OS or any other Arch based distros.

The script offers only opensource video drivers!

# For Arch based distros

Make sure to run:

`sudo nano /etc/pacman.conf`

and scroll down and uncomment every repository there is excluding testing repos then run:

`sudo pacman -Sy`


# PISS INFO

PISS will deploy my dotfiles to your freshly installed system

# Programs

PISS will install the following programs:

1. My patched DWM (Dynamic Window Manager)
2. My patched ST (Simple Terminal)
3. My patched DWMBLOCKS (Dynamic Window Manager Blocks)
4. Firefox
5. Mplayer (Music Player)
6. Wine (Windows Compatibility Layer)
7. Grapejuice (Helper to run Roblox Game on Linux)
8. Doom Emacs (Text Editor)
9. Latest Xanmod Kernel (Will be compiled at the end of the script)
10. Pipewire (PulseAudio Alternative)
11. Some Utils (Htop, Gotop, Awk)
12. Fish Shell (Shell)
13. Neofetch (Must have!!)
14. LightDM (Light Display Manager)

# Note

You should not run this script as root because it will make changes to your /home/(user) directory.
